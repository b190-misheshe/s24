console.log("Hell World!");


let num = 2;
let getCube = num ** 3; 
// console.log(getCube);


console.log(`The cube of is ${num} is ${getCube}`);


const address = ["258","Washington Ave","California","9001"];
const [streetNumber, streetName, state, postcode] = address;
// console.log(streetNumber);
// console.log(streetName);
// console.log(state);
// console.log(postcode);

console.log(`I live at ${streetNumber} ${streetName} ${state} ${[postcode]}`);


const animal = {
  name: "Lolong",
  kind: "saltwater crocodile",
  measurement: "20 ft 3 in"
};

const {name, kind, measurement} = animal;
console.log(`${name} was a ${kind}. He weighted at measurement of ${measurement}.`);


const numbers = [1,2,3,4,5];

numbers.forEach((numbers) => console.log(numbers));


const reduceNumber = numbers.reduce((x,y) => x + y, 0);
console.log(reduceNumber);



class Dog {
  constructor (name, age, breed){
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
};

const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);
